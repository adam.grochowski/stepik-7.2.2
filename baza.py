from neo4j import GraphDatabase


class BazaDanych:

    def __init__(self, uri, user, password):
        self.handle = GraphDatabase.handle(uri, auth=(user, password))

    def uzyskajPracownikow(self):
        with self.handle.session() as session:
            return session.read_transaction(self._uzyskaj_Pracownikow)

    @staticmethod
    def _uzyskaj_Pracownikow(tx):
        odp = tx.run('MATCH (m:Employee) RETURN m').data()
        return odp

    def dodajPracownika(self, name, department):
        with self.handle.session() as session:
            return session.execute_write(self._dodaj_pracownika, name, department)

    @staticmethod
    def _dodaj_pracownika(tx, name, department):
        odp = tx.run("CREATE (a:Employee) "
                        "SET a.name = $name, "
                        "SET a.department = $department "
                        "RETURN a", name=name, department=department)
        return odp.single()

    def usunPracownika(self, id):
        with self.handle.session() as session:
            return session.write_transaction(self._usun_pracownika, id)

    @staticmethod
    def _usun_pracownika(tx, id):
        odp = tx.run('MATCH (m:Employee) WHERE m.id=$id DETACH DELETE m', id=id)
        return odp

    def uzyskajPodwladnych(self, id):
        with self.handle.session() as session:
            return session.read_transaction(self._uzyskaj_podwladnych, id)

    @staticmethod
    def _uzyskaj_podwladnych(tx, id):
        odp = tx.run('MATCH (m: Employee { id: $id })-[:MANAGES]->(who: Employee) RETURN who', id=id).data()
        return odp

    def uzyskajDepartamentPracownika(self, id):
        with self.handle.session() as session:
            return session.read_transaction(self._uzyskajDepartamentPracownika, id)

    @staticmethod
    def _uzyskajDepartamentPracownika(tx, id):
        odp = tx.run('MATCH (d: Department)<-[:WORKS_IN]-(e: Employee{ id: $id }) RETURN d', id=id).data()
        return odp

    def uzyskajDepartamenty(self):
        with self.handle.session() as session:
            return session.read_transaction(self._uzyskajDepartamenty)

    @staticmethod
    def _uzyskajDepartamenty(tx):
        odp = tx.run('MATCH (d: Department) RETURN d').data()
        return odp

    def uzyskajPracownikowDepartamentu(self, id):
        with self.handle.session() as session:
            return session.read_transaction(self._uzyskajPracownikowDepartamentu, id)

    @staticmethod
    def _uzyskajPracownikowDepartamentu(tx, id):
        odp = tx.run('MATCH (e: Employee)-[:WORKS_IN]->(d: Department{ id: $id }) RETURN e', id=id).data()
        return odp
from flask import Flask, jsonify, request
from baza import BazaDanych
app = Flask(__name__)
db = BazaDanych("uri", "user", "password")


@app.route('/departments/all', methods=['GET'])
def uzyskajDepartamenty():
    departamenty = db.uzyskajDepartamenty()
    odp = [{'departament': x['d']} for x in departamenty]
    return jsonify(odp)


@app.route("/employees/:id", methods=['PUT'])
def edytujPracownika():
    dane = request.get_json()
    return jsonify(dane)


@app.route('/employees/:id/subordinates', methods=['GET'])
def uzyskajPodwladnych(id):
    odp = db.uzyskajPodwladnych(id)
    return odp['who']


@app.route("/employees", methods=['POST'])
def dodajPracownika():
    dane = request.get_json()
    imie = dane['name']
    departament = dane['department']
    odp = db.dodajPracownika(imie, departament)
    return jsonify(odp)


@app.route('/employees/:id/department', methods=['GET'])
def uzyskajDepartamentPracownika(id):
    odp = db.uzyskajDepartamentPracownika(id)
    return odp


@app.route('/departments/:id/employees', methods=['GET'])
def uzyskajPracownikowDepartamentu(id):
    pracownicy = db.uzyskajPracownikowDepartamentu(id)
    odp = [{'pracownik': x['e']} for x in pracownicy]
    return jsonify(odp)


@app.route("/employees", methods=['GET'])
def uzyskajPracownikow():
    pracownicy = db.uzyskajPracownikow()
    odp = [{'imie': x['m']['name']} for x in pracownicy]
    return jsonify(odp)


@app.route('/employees/:id', methods=["DELETE"])
def usunPracownika(id):
    odp = db.usunPracownika(id)
    if odp:
        res = {'status': 'pomyslnie usunieto'}
        return jsonify(res)
    else:
        res = {'message': 'Nie znaleziono pracownika'}
        return jsonify(res), 404
